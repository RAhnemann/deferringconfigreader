﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Xml;
using Sitecore.Configuration;
using Sitecore.Diagnostics;

namespace Sitecore.SharedSource.DeferringConfigReader
{
    public class DeferringConfigReader : ConfigReader
    {
        private List<string> _configDefers;

        protected override void LoadAutoIncludeFiles(XmlNode element)
        {
            Assert.ArgumentNotNull(element, "element");
            ConfigPatcher configPatcher = GetConfigPatcher(element);
            LoadAutoIncludeFiles(configPatcher, MainUtil.MapPath("/App_Config/Sitecore/Components"));
            LoadAutoIncludeFiles(configPatcher, MainUtil.MapPath("/App_Config/Include"));

            ProcessDeferrals(configPatcher);
        }


        protected override void LoadAutoIncludeFiles(ConfigPatcher patcher, string folder)
        {
            Assert.ArgumentNotNull(patcher, "patcher");
            Assert.ArgumentNotNull(folder, "folder");

            if (_configDefers == null)
            {
                InintializeDeferrals();
            }

            try
            {
                if (!Directory.Exists(folder))
                    return;

                foreach (string str in Directory.GetFiles(folder, "*.config"))
                {
                    try
                    {
                        if ((File.GetAttributes(str) & FileAttributes.Hidden) == 0)
                        {
                            if (_configDefers != null)
                            {
                                var configItem = _configDefers.Find(f => f.Equals(str, StringComparison.InvariantCultureIgnoreCase));

                                if (configItem != null)
                                {
                                    //We found the item based off path. Skip loading it for now
                                    if (File.Exists(configItem))
                                    {
                                        Log.Info("Deferring config: " + str, this);
                                        continue;
                                    }
                                }
                            }

                            patcher.ApplyPatch(str);
                        }

                    }
                    catch (Exception ex)
                    {
                        Log.Error(string.Concat("Could not load configuration file: ", str, ": ", ex), typeof(Factory));
                    }
                }
                foreach (string str in Directory.GetDirectories(folder))
                {
                    try
                    {
                        if ((File.GetAttributes(str) & FileAttributes.Hidden) == 0)
                            LoadAutoIncludeFiles(patcher, str);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(string.Concat("Could not scan configuration folder ", str, " for files: ", ex), typeof(Factory));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(string.Concat("Could not scan configuration folder ", folder, " for files: ", ex), typeof(Factory));
            }
        }

        private void InintializeDeferrals()
        {
            Log.Info("Initializing config defer", this);

            _configDefers = new List<string>();

            var configName = HttpContext.Current.Server.MapPath("~/App_Config/ConfigsToDefer.config");

            if (File.Exists(configName))
            {
                Log.Info("Found config defer file at: " + configName, this);

                try
                {
                    var deferConfig = new XmlDocument();

                    deferConfig.Load(configName);

                    var configNodes = deferConfig.SelectNodes("/FileList/File");

                    if (configNodes != null)
                    {
                        foreach (XmlNode configNode in configNodes)
                        {
                            if (configNode.Attributes != null && configNode.Attributes["name"] == null)
                            {
                                Log.Warn("Couldn't find name on deferrable config item", this);
                                continue;
                            }

                            if (configNode.Attributes != null && configNode.Attributes["name"] != null)
                                _configDefers.Add(HttpContext.Current.Server.MapPath(configNode.Attributes["name"].Value));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Couldn't load deferrable config from: " + configName, ex, this);
                }
            }
            else
            {
                Log.Warn("Couldn't find defer config at: " + configName, this);
            }
        }

        private void ProcessDeferrals(ConfigPatcher configPatcher)
        {
            Assert.ArgumentNotNull(configPatcher, "configPatcher");

            foreach (var config in _configDefers)
            {
                if (File.Exists(config))
                {
                    Log.Info("Applying Deferred config: " + config, this);
                    configPatcher.ApplyPatch(config);
                }
                else
                {
                    Log.Warn("Skipping config. Doesn't exist.  Config file: " + config, this);
                }
            }
        }
    }
}
