# README #

### Installation Steps:  ###

 
1.  Copy the dll to the bin folder  
2.  Copy ConfigsToDefer.config to /App_Config/ConfigsToDefer.config  
3.  Add files using relative pathing ("eg: ~/App_Config/Include/XXXXXXXXX.config)  
4.  Modify the web.config to replace the sitecore section:  
  
```
#!xml

<section name="sitecore" type="Sitecore.SharedSource.DeferringConfigReader.DeferringConfigReader, Sitecore.SharedSource.DeferringConfigReader" />
```